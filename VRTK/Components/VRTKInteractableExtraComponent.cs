﻿#if VRTK
namespace d4160.Adapters.VRTK
{
    using UnityEngine;
    using ECS.Component;
    
    /// <summary>
    /// The public interface into the Interactable Prefab.
    /// </summary>
    public class VRTKInteractableExtraComponent //: ComponentDataWrapper<VRTKInteractableExtra>
    {
    }

    [System.Serializable]
    public struct VRTKInteractableExtra //: IComponentData
    {
        /// <summary>
        /// The touch type when touch the Interactable, only as a indicator variable.
        /// </summary>
        [Tooltip("The touch type when touch the Interactable, only as a indicator variable.")]
        public TouchType touchType;
    }
    
    public enum TouchType
    {
        Touch,
        NearTouch
    }
}
#endif