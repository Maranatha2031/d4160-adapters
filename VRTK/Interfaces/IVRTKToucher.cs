﻿#if VRTK
using d4160.ECS;
using VRTK.Prefabs.Interactions.Interactables;

namespace d4160.Adapters.VRTK
{
    public interface IVRTKToucher : IInteractor
    {
        void OnTouched(InteractableFacade interactable);

        void OnUntouched(InteractableFacade interactable);
    }
}
#endif