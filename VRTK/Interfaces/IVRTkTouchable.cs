﻿#if VRTK
using d4160.ECS;
using VRTK.Prefabs.Interactions.Interactors;

namespace d4160.Adapters.VRTK
{
    public interface IVRTKTouchable : IInteractable
    {
        void OnTouched(InteractorFacade grabber);

        void OnUntouched(InteractorFacade grabber);
    }
}
#endif