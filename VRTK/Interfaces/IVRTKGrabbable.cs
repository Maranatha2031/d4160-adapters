﻿#if VRTK
using d4160.ECS;
using VRTK.Prefabs.Interactions.Interactors;

namespace d4160.Adapters.VRTK
{
    public interface IVRTKGrabbable : IInteractable
    {
        void OnGrabbed(InteractorFacade grabber);

        void OnUngrabbed(InteractorFacade grabber);

        void ForceUngrab();
    }
}
#endif