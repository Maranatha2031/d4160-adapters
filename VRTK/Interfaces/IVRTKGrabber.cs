﻿#if VRTK
using d4160.ECS;
using VRTK.Prefabs.Interactions.Interactables;

namespace d4160.Adapters.VRTK
{
    public interface IVRTKGrabber : IInteractor
    {
        void OnGrabbed(InteractableFacade interactable);

        void OnUngrabbed(InteractableFacade interactable);
    }
}
#endif