#if B_BRICKS
using UnityEngine;

namespace d4160.ECS
{
    [RequireComponent(typeof(BehaviorExecutor))]
    public abstract class BBricksAI<T, AI> : AILogic<T, AI> where T : IComponentData where AI : IAI
    {
        [Header("Options")]
        [SerializeField]
        protected TickOnType m_tickOnType;
        protected BehaviorExecutor m_bBricks;

        public override TickOnType TickOnType
        {
            get
            {
                return m_tickOnType;
            }
        }

        protected override void CatchComponents()
        {
            m_bBricks = GetComponent<BehaviorExecutor>();
        }
    }
}
#endif