#if SURGE
using Pixelplacement;

namespace d4160.Clon.Pong
{
    public abstract class SurgeState<FSM> : State where FSM : StateMachine
    {
        protected FSM m_machine;

        private void OnEnable()
        {
            if (!m_machine)
                m_machine = StateMachine as FSM;

            UpdateState();
        }

        public abstract void UpdateState();
    }
}
#endif