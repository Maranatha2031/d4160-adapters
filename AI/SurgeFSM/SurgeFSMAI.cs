#if SURGE
namespace d4160.ECS
{
    using Component;
    using UnityEngine;
    using Pixelplacement;
    
    public abstract class SurgeFSMAI<T, TAi> : StateMachine, IAILogic where TAi : IAI
    {
        [SerializeField] protected T m_serializedData;
        [Header("Options")]
        [SerializeField] protected TickType m_tickOnType;
        
        protected TAi m_ai;

        public virtual TickType TickOnType => m_tickOnType;

        public virtual T SerializedData
        {
            get => m_serializedData;
            set => m_serializedData = value;
        }
        
        public virtual TAi EntityAi
        {
            get => m_ai;
            set => m_ai = value;
        }

        protected virtual void Awake()
        {
            CatchComponents();
        }

        protected virtual void OnEnable()
        {
            Init();
        }

        /// <summary>
        /// Use to enable or disable AI for this entity
        /// </summary>
        /// <param name="active"></param>
        /// <param name="layerMask"></param>
        public abstract void SetActive(bool active, int layerMask = 0);
        
        /// <summary>
        /// To catch components and set values only once on all life of the game
        /// </summary>
        protected virtual void CatchComponents() { }
        
        /// <summary>
        /// To init changing values OnEnable, so can be set new values to reload and reassign
        /// </summary>
        public virtual void Init() { }
        
        /// <summary>
        /// Tick of "Thinking" of the AIAgent
        /// </summary>
        protected abstract void Tick();
    }
}
#endif