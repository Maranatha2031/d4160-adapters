#if ENABLE_TENSORFLOW
using MLAgents;
using UnityEngine;

namespace d4160.ECS
{
    public abstract class MLAgentAI<T, AI> : Agent, IAILogic where T : IComponentData where AI : IAI
    {
        [SerializeField]
        protected T m_serializedData;
        protected AI m_ai;

        public virtual T SerializedData {
            get {
                return m_serializedData;
            }
            set {
                m_serializedData = value;
            }
        }
        public virtual AI EntityAI
        {
            get
            {
                return m_ai;
            }
            set
            {
                m_ai = value;
            }
        }
        public virtual TickOnType TickOnType
        {
            get
            {
                return TickOnType.Update;
            }
        }

        protected virtual void Awake()
        {
            CatchComponents();
        }

        protected override void OnEnable()
        {
            // Set virtual on Agent class
            base.OnEnable();

            Init();
        }

        /// <summary>
        /// Use to enable or disable AI for this entity
        /// </summary>
        /// <param name="active"></param>
        /// <param name="inputLayerMask"></param>
        public abstract void SetActive(bool active, int aiLayerMask = 0);
        /// <summary>
        /// To catch components and set values only once on all life of the game
        /// </summary>
        protected virtual void CatchComponents() { }
        /// <summary>
        /// To init changing values OnEnable, so can be set new values to reload and reassign
        /// </summary>
        public virtual void Init() { }
        /// <summary>
        /// Tick of "Thinking" of the AIAgent
        /// </summary>
        protected abstract void Tick();
    }
}
#endif