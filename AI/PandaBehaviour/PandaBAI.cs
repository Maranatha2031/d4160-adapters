#if PANDA_B
using UnityEngine;
using Panda;
using static Panda.BehaviourTree;

namespace d4160.ECS
{
    [RequireComponent(typeof(PandaBehaviour))]
    public abstract class PandaBAI<T, AI> : AILogic<T, AI> where T : IComponentData where AI : IAI
    {
        [Header("Options")]
        [SerializeField]
        protected TickOnType m_tickOnType;
        protected PandaBehaviour m_pandaB;

        public override TickOnType TickOnType
        {
            get
            {
                return m_tickOnType;
            }
        }

        protected override void CatchComponents()
        {
            m_pandaB = GetComponent<PandaBehaviour>();
        }
    }
}
#endif