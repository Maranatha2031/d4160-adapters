#if ANIMATOR_FSM
using UnityEngine;
using AnimatorUtility;

namespace d4160.ECS
{
    [RequireComponent(typeof(Animator))]
    public abstract class AnimatorFSMAI<T, AI> : AILogic<T, AI> where T : IComponentData where AI : IAI 
    {
        [Header("Options")]
        [SerializeField]
        protected TickOnType m_tickOnType;
        protected Animator m_animator;
        protected AnimatorStateMachineUtil m_animUtil;

        public override TickOnType TickOnType
        {
            get
            {
                return m_tickOnType;
            }
        }

        protected override void CatchComponents()
        {
            m_animator = GetComponent<Animator>();
            m_animUtil = GetComponent<AnimatorStateMachineUtil>();
        }
    }
}
#endif