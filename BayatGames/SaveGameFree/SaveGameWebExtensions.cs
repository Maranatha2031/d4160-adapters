﻿#if BAYAT_GAMES
using UnityEngine;
using System.Collections;
using BayatGames.SaveGameFree;
using System;

namespace d4160.Adapters.BayatGames
{
    public static class SaveGameWebExtensions
    {
        /// <summary>
		/// Download the specified identifier.
		/// </summary>
		/// <param name="identifier">Identifier.</param>
		public static IEnumerator Download(this SaveGameWeb e, string identifier, Action afterExecutionCallback)
        {
            yield return e.Send(identifier, null, "load");
            if (e.m_IsError)
            {
                Debug.LogError(e.m_Error);
            }
            else
            {
                Debug.Log("Data successfully downloaded.");
            }

            afterExecutionCallback?.Invoke();
        }
    }
}
#endif