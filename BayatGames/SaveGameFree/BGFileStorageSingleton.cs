﻿#if BAYAT_GAMES
using BayatGames.SaveGameFree;
using BayatGames.SaveGameFree.Serializers;

namespace d4160.Adapters.BayatGames
{
    using UnityEngine;
    using System.Collections;
    using System.Threading.Tasks;
    using d4160.Singletons;

    /// <summary>
    /// Depends on BayatGames.SaveGameFree 
    /// </summary>
    public class BGFileStorageSingleton : DataStorageSingleton<BGFileStorageSingleton>
    {
        private static readonly ISaveGameSerializer[] _Serializers = new ISaveGameSerializer[] {
            new SaveGameXmlSerializer (),
            new SaveGameJsonSerializer (),
            new SaveGameBinarySerializer ()
        };

        private BGFileStorageOptions _storageOptions;

        /// <summary>
        /// The Storage Options used to Save and Load data.
        /// Make sure to set this correctly before the Save or Load operation
        /// This way allow Load and Save for/from/to different sources 
        /// </summary>
        public BGFileStorageOptions StorageOptions { get => _storageOptions; set => _storageOptions = value; }

        public ISaveGameSerializer ActiveSerializer
        {
            get
            {
                switch (_storageOptions.serializer)
                {
                    case SaveSerializer.XML:
                        return _Serializers[0];
                    case SaveSerializer.JSON:
                        return _Serializers[1];
                    case SaveSerializer.Binary:
                        return _Serializers[2];
                    default:
                        return _Serializers[0];
                }
            }
        }

        /// <summary>
        /// Check if local data exists using the 'identifier' of StorageOptions (make sure is correct set)
        /// If the data is in the web, don't use this to avoid Load
        /// </summary>
        /// <returns></returns>
        public override bool LocalDataExists => SaveGame.Exists(_storageOptions.identifier);

        /// <summary>
        /// Save the data to the file using the StorageOptions
        /// Make sure set the correct data before
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        public override void Save<T>(T obj)
        {
            SaveGame.Encode = _storageOptions.encode;
            SaveGame.EncodePassword = _storageOptions.encodePassword;
            SaveGame.SavePath = GetGamePath(_storageOptions.savePath);

            if (_storageOptions.savePath != SavePath.Web)
                SaveGame.Save(_storageOptions.identifier, obj, ActiveSerializer);
            else
            {
                StartCoroutine(SaveCo(obj));
            }
        }

        protected override IEnumerator SaveCo<T>(T obj)
        {
            Debug.Log("Uploading...");
            SaveGameWeb web = new SaveGameWeb(
                                  _storageOptions.username,
                                  _storageOptions.password,
                                  _storageOptions.url,
                                  _storageOptions.encode,
                                  _storageOptions.encodePassword,
                                  ActiveSerializer);

            yield return StartCoroutine(web.Save<T>(_storageOptions.identifier, obj));
            Debug.Log("Upload Done.");
        }

        /// <summary>
        /// Load the data of the file using the StorageOptions
        /// Make sure set the correct data before
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public async override Task<T> Load<T>()
        {
            SaveGame.Encode = _storageOptions.encode;
            SaveGame.EncodePassword = _storageOptions.encodePassword;
            SaveGame.SavePath = GetGamePath(_storageOptions.savePath);

            if (_storageOptions.savePath != SavePath.Web)
            {
                T data = SaveGame.Load<T>(_storageOptions.identifier, default, ActiveSerializer);

                return data;
            }

            return await LoadAsync<T>();
        }

        protected async override Task<T> LoadAsync<T>()
        {
            Debug.Log("Downloading...");
            SaveGameWeb web = new SaveGameWeb(
                                  _storageOptions.username,
                                  _storageOptions.password,
                                  _storageOptions.url,
                                  _storageOptions.encode,
                                  _storageOptions.encodePassword,
                                  ActiveSerializer);

            var tcs = new TaskCompletionSource<object>();

            StartCoroutine(web.Download(_storageOptions.identifier,
                    () => tcs.TrySetResult(null)));

            await tcs.Task;

            Debug.Log("Download Done.");

            return web.Load<T>(_storageOptions.identifier, default);
        }

        private SaveGamePath GetGamePath(SavePath path)
        {
            switch (path)
            {
                case SavePath.PersistentDataPath:
                    return SaveGamePath.PersistentDataPath;
                case SavePath.DataPath:
                    return SaveGamePath.DataPath;
                default:
                    return SaveGamePath.PersistentDataPath;
            }
        }
    }

    [System.Serializable]
    public struct BGFileStorageOptions
    {
        public SavePath savePath;
        public string identifier;
        public SaveSerializer serializer;
        public bool encode;
        public string encodePassword;
        [Header("Web Only")]
        public string username;
        public string password;
        public string url;

        public BGFileStorageOptions(bool de = true)
        {
            savePath = SavePath.PersistentDataPath;
            identifier = "save.dat";
            serializer = SaveSerializer.Binary;
            encode = true;
            encodePassword = "2@#^0$#^#3@!^*1";
            username = "myUsername";
            password = "myPassword";
            url = "http://www.example.com/player.php";
        }
    }
}
#endif
