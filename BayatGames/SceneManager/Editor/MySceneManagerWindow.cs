﻿/*using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BayatGames.Utilities.Editor;
using UnityEditor;

namespace d4160.Adapters.BayatGames.SceneManager
{
    public class MySceneManagerWindow : SceneManagerWindow
    {
        [MenuItem("Tools/Complete Scene Manager")]
        public static void MyInit()
        {
            var window = EditorWindow.GetWindow<MySceneManagerWindow>("Scene Manager");
            window.minSize = new Vector2(400f, 200f);
            window.Show();
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            tabs = new string[] {
                "My Scenes",
                "Categories",
                "All Scenes",
                "Settings"
            };
        }


        protected override void OnGUI()
        {
            EditorGUILayout.BeginHorizontal(EditorStyles.toolbar);
            this.selectedTab = GUILayout.Toolbar(this.selectedTab, this.tabs, EditorStyles.toolbarButton);
            EditorGUILayout.EndHorizontal();
            this.scrollPosition = EditorGUILayout.BeginScrollView(this.scrollPosition);
            EditorGUILayout.BeginVertical();
            switch (this.selectedTab)
            {
                case 0:
                    break;
                case 1:
                    break;
                case 2:
                    ScenesTabGUI();
                    break;
                case 3:
                    SettingsTabGUI();
                    break;
            }
            EditorGUILayout.EndVertical();
            EditorGUILayout.EndScrollView();
            GUILayout.Label("Made with ❤️ by Bayat Games", EditorStyles.centeredGreyMiniLabel);
        }
    }
}*/