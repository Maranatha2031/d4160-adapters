﻿#if OCULUS
using UnityEngine;

namespace d4160.Adapters.Oculus
{
    // Staus codes that may return from Ambisonic engine
    public enum ovrAmbisonicsNativeStatus
    {
        Uninitialized = -1,     // Ambisonic stream not initialized (inital status)
        NotEnabled,             // Ambisonic has not been enabled on clip 
        Success,                // Stream initialized and playing
        StreamError,            // Something wrong with input stream (not a 4-channel AmbiX format stream?)
        ProcessError,           // Handling of stream error
        MaxStatValue
    };

    [RequireComponent(typeof(AudioSource))]
    public class ONSPAmbisonicsNative : MonoBehaviour
    {
        static int numFOAChannels = 4;  // we are only dealing with 1st order Ambisonics at this time
        static int paramVSpeakerMode = 6;  // set speaker mode (OculusAmbi or VSpeaker)
        static int paramAmbiStat = 7;  // use this to return internal Ambisonic status

        // true to use Virtual Speaker output. Otherwise use OculusAmbi
        [SerializeField]
        private bool _useVirtualSpeakers = false;
        private AudioSource _source;
        // current status
        ovrAmbisonicsNativeStatus _currentStatus = ovrAmbisonicsNativeStatus.Uninitialized;

        public bool UseVirtualSpeakers
        {
            get
            {
                return _useVirtualSpeakers;
            }
            set
            {
                _useVirtualSpeakers = value;
            }
        }

        /// <summary>
        /// OnEnable this instance.
        /// </summary>
        private void Awake()
        {
            _source = GetComponent<AudioSource>();

        }

        public void SetActive(bool active)
        {
            enabled = active;

            if (active)
            {
                CheckStatus();

                UpdateOnce();
            }
            else
            {
                _currentStatus = ovrAmbisonicsNativeStatus.Uninitialized;
            }
        }

        private void CheckStatus()
        {
            _currentStatus = ovrAmbisonicsNativeStatus.Uninitialized;

            if (_source == null)
            {
                Debug.Log("Ambisonic ERROR: AudioSource does not exist.");
            }
            else
            {
                if (_source.spatialize == true)
                {
                    Debug.Log("Ambisonic WARNING: Turning spatialize field off for Ambisonic sources.");
                    _source.spatialize = false;
                }

                if (_source.clip == null)
                {
                    Debug.Log("Ambisonic ERROR: AudioSource does not contain an audio clip.");
                }
                else
                {
                    if (_source.clip.channels != numFOAChannels)
                    {
                        Debug.Log("Ambisonic ERROR: AudioSource clip does not have correct number of channels.");
                    }
                }
            }
        }

        /// <summary>
        /// Update this instance.
        /// </summary>
        private void UpdateOnce()
        {
            // Set speaker mode
            if (_useVirtualSpeakers == true)
                _source.SetAmbisonicDecoderFloat(paramVSpeakerMode, 1.0f); // VSpeakerMode
            else
                _source.SetAmbisonicDecoderFloat(paramVSpeakerMode, 0.0f); // OclusAmbi 

            float statusF = 0.0f;
            // PGG 5/25/2017 There is a bug in the 2017.2 beta that does not
            // allow for ambisonic params to be passed through to native
            // from C# Get latest editor from Unity when available
            _source.GetAmbisonicDecoderFloat(paramAmbiStat, out statusF);

            ovrAmbisonicsNativeStatus status = (ovrAmbisonicsNativeStatus)statusF;

            // TODO: Add native result/error codes
            if (status != _currentStatus)
            {
                switch (status)
                {
                    case (ovrAmbisonicsNativeStatus.NotEnabled):
                        Debug.Log("Ambisonic Native: Ambisonic not enabled on clip. Check clip field and turn it on");
                        break;

                    case (ovrAmbisonicsNativeStatus.Uninitialized):
                        Debug.Log("Ambisonic Native: Stream uninitialized");
                        break;

                    case (ovrAmbisonicsNativeStatus.Success):
                        Debug.Log("Ambisonic Native: Stream successfully initialized and playing/playable");
                        break;

                    case (ovrAmbisonicsNativeStatus.StreamError):
                        Debug.Log("Ambisonic Native WARNING: Stream error (bad input format?)");
                        break;

                    case (ovrAmbisonicsNativeStatus.ProcessError):
                        Debug.Log("Ambisonic Native WARNING: Stream process error (check default speaker setup)");
                        break;

                    default:
                        break;
                }
            }

            _currentStatus = status;
        }
    }

}
#endif