﻿#if OCULUS
namespace d4160.Adapters.Oculus
{
    using UnityEngine;
    using ECS.Component;
    
    [RequireComponent(typeof(AudioSource))]
    [RequireComponent(typeof(ONSPAudioSource))]
    [RequireComponent(typeof(ONSPAmbisonicsNative))]
    public class OVRAudioEmitter : AudioEmitterComponent
    {
        protected ONSPAudioSource m_osp;
        protected ONSPAmbisonicsNative m_oabs;

        public ONSPAudioSource ONSPAudioSource => m_osp;

        public ONSPAmbisonicsNative ONSPAmbisonics => m_oabs;

        protected override void Awake()
        {
            base.Awake();

            m_aSource = GetComponent<AudioSource>();
        }

        public override void SetActive(bool active)
        {
            base.SetActive(active);

            m_osp.SetActive(active);

            m_oabs.SetActive(active);
        }
    }
}
#endif