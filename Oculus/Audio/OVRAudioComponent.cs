﻿#if OCULUS
namespace d4160.Adapters.Oculus
{
    using ECS.Component;
    using UnityEngine;
    using Extensions;
    using OVR;

    public class OVRAudioComponent : AudioComponent<OVRAudio>
    {
        protected override Transform PlayPosition => null;//m_serializedData.playPositions[Random.Range(0, m_serializedData.playPositions.Length)];
    }

    [System.Serializable]
    public struct OVRAudio : IAudioData
    {
        public SoundFXRef[] sfxs;
        public Transform[] playPositions;

        public void PlayAt(Vector3 point, int idx)
        {
            if (!sfxs.IsValidIndex(idx))
                return;

            sfxs[idx].PlaySoundAt(point);
        }

        public void Play(int idx)
        {
            if (sfxs.IsValidIndex(idx))
                return;

            sfxs[idx].PlaySound();
        }

        public void PlayAt(Transform parent, int idx = 0)
        {
            sfxs[idx].PlaySoundAt(parent.position);
            sfxs[idx].AttachToParent(parent);
        }
    }
}
#endif