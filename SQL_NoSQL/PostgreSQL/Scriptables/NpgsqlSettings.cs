﻿#if NPGSQL
using UnityEngine;

namespace d4160.Adapters.Npgsql
{
    [CreateAssetMenu(fileName = "NpgsqlSettings", menuName = "Scriptable/Adapters/Npgsql/NpgsqlSettings")]
    public class NpgsqlSettings : ScriptableObject
    {
        public string host = "";
        public string port = "";
        public string database = "";
        public string username = "";
        public string password = "";
    }
}
#endif