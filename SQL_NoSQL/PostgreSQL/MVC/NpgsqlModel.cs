﻿#if NPGSQL
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace d4160.Adapters.Npgsql
{
    /* http://www.postgresqltutorial.com/postgresql-data-types/ */
    public enum PostgreSQLDataType
    {
        boolean,
        char_n,
        varchar_n,
        text,
        int2,
        int4,
        int8,
        serial2,
        serial4,
        serial8,
        float4,
        float8,
        numeric, // _presicion_scale: 7,3: 1234.567
        date,
        time, // p
        timetz, // p
        timestamp,
        timestamptz,
        json,
        jsonb,
        uuid,
        inet,
        macaddr
    }

    public class NpgsqlModel : MonoBehaviour
    {
        [Serializable]
        public struct SQLColumn
        {
            [Tooltip("Use the form: user_id, username (lowercase)")]
            public string name;
            public PostgreSQLDataType dataType;
            [Tooltip("For DataTypes that need a length like char, varchar, time.")]
            public int optionalLenght1;
            [Tooltip("For DataTypes that need a two lengths like numeric.")]
            public int optionalLength2;
            [Tooltip("Constraint for this column as NOT NULL, UNIQUE, PRIMARY KEY, etc")]
            public string constraints;

            public string DataTypeString => GetCorrectString(dataType, optionalLenght1, optionalLength2);
        }

        [Serializable]
        public struct SQLForeignKey
        {
            // foreignColumn
            [Tooltip("Use the form: account, role (lowercase)")]
            public string foreignTable;
            // onTable
            [Tooltip("The column name need to be the same on both tables")]
            public string columnName;
            public string optionalConstraints;
        }

        public static string GetCorrectString(PostgreSQLDataType dataType, int length1 = 0, int length2 = 0)
        {
            string val = dataType.ToString();

            if (length1 == 0 || (length1 != 0 && length2 == 0))
            {
                switch (dataType)
                {
                    case PostgreSQLDataType.char_n:
                        val = $"char(1)";
                        break;
                    case PostgreSQLDataType.varchar_n:
                        val = $"varchar(30)";
                        break;
                }

                return val;
            }

            switch (dataType)
            {
                case PostgreSQLDataType.char_n:
                    val = $"char({length1})";
                    break;
                case PostgreSQLDataType.varchar_n:
                    val = $"varchar({length1})";
                    break;
                case PostgreSQLDataType.numeric:
                    val = $"numeric({length1},{length2})";
                    break;
                case PostgreSQLDataType.time:
                    val = $"time({length1})";
                    break;
                case PostgreSQLDataType.timetz:
                    val = $"timetz({length1})";
                    break;
                default:
                    break;
            }

            return val;
        }
    }
}
#endif