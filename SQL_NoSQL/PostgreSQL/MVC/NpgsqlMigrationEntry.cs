﻿#if NPGSQL
using d4160.MVC;
using UnityEngine;

namespace d4160.Adapters.Npgsql
{
    [System.Serializable]
    public struct NpgsqlMigrationEntry
    {
        public string tableName;
        [Tooltip("Keep empty when use a Column Constraint. Separate with commas for multi primary keys.")]
        public string primaryKeyColumn;
        [Space]
        public NpgsqlModel.SQLColumn[] columns;
        public NpgsqlModel.SQLForeignKey[] foreignKeys;

        public NpgsqlMigrationEntry(string tableName, string[] primaryKeys, string[] columnArray, string[,] foreignKeys)
        {
            this.tableName = tableName;
            primaryKeyColumn = Model.GetPrimaryKeysString(primaryKeys);
            columns = new NpgsqlModel.SQLColumn[columnArray.Length];
            this.foreignKeys = new NpgsqlModel.SQLForeignKey[foreignKeys.GetLength(0)];

            for (int i = 0; i < columnArray.Length; i++)
            {
                columns[i].name = columnArray[i];
            }

            for (int i = 0; i < foreignKeys.GetLength(0); i++)
            {
                this.foreignKeys[i].foreignTable = foreignKeys[i, 0];
                this.foreignKeys[i].columnName = foreignKeys[i, 1];
            }
        }
    }
}
#endif