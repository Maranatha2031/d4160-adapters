﻿#if NPGSQL
using d4160.ECS;
using UnityEngine;
using Npgsql;
using System;
using System.Threading.Tasks;
using SqlKata.Compilers;
using SqlKata;
using SqlKata.Execution;

namespace d4160.Adapters.Npgsql
{
    /// <summary>
    /// Singleton to work with PostgreSQL
    /// </summary>
    public class NpgsqlSingleton : SQLStorageSingleton<NpgsqlSingleton>
    {
        #region Fields
        [SerializeField]
        protected NpgsqlSettings m_settings;

        protected NpgsqlConnection m_conn;
        protected QueryFactory m_db;
        #endregion

        #region Properties
        public NpgsqlConnection Connection
        {
            get
            {
                if (m_conn == null)
                    OpenConnection();
                return m_conn;
            }
        }

        public QueryFactory DB {
            get {
                if (m_db == null)
                    OpenConnection();

                return m_db;
            }
        }
        #endregion

        #region Events
        public static event Action<string> OnConnected;
        public static event Action<string> OnDisconnected;
        public static event Action<string, string> OnCommandExecuted;
        #endregion

        #region Unity methods
        protected void OnEnable()
        {
            if (m_connectOnEnable)
                OpenConnection();
        }

        protected void OnDisable()
        {
            CloseConnection();
        }
        #endregion

        #region Open/Close methods
        /// <summary>
        /// Open DB Connection
        /// </summary>
        /// <param name="force">Force open connection again even it's already openned</param>
        public override void OpenConnection(bool force = false)
        {
            if (!force)
            {
                if (m_conn != null)
                    return;
            }

            string connectString = 
                $@"Host={m_settings.host};
Port={m_settings.port};
Database={m_settings.database};
Username={m_settings.username};
Password={m_settings.password};
SslMode=Require;";

            try
            {
                m_conn = new NpgsqlConnection(connectString);
                m_conn.Open();

                var txt = "Connection correctly openned.";

                OnConnected?.Invoke(txt);

                if (logging)
                    Debug.Log(txt);

            }
            catch (Exception ce)
            {
                if (logging)
                    Debug.Log(ce.ToString());
            }

            var compiler = new PostgresCompiler();

            m_db = new QueryFactory(m_conn, compiler);

            if (logging)
                m_db.Logger = compiled => {
                    Debug.Log(compiled.ToString());
                };
        }

        public override void CloseConnection()
        {
            if (m_conn != null)
            {
                m_conn.Close();

                m_conn.ClearPool();
                
                m_conn.Dispose();

                m_conn = null;

                var txt = "Connection was closed.";

                OnDisconnected?.Invoke(txt);

                if (logging)
                    Debug.Log(txt);
            }
        }
        #endregion

        #region Create and Drop methods
        /// <summary>
        /// http://www.postgresqltutorial.com/postgresql-create-table/
        /// </summary>
        /// <param name="tableName">account, role, account_role (lowercase)</param>
        /// <param name="columns"></param>
        /// <param name="tablePrimaryKeyColumn"></param>
        /// <param name="foreignKeys"></param>
        public void CreateTable(string tableName, NpgsqlModel.SQLColumn[] columns, string tablePrimaryKeyColumn = null, NpgsqlModel.SQLForeignKey[] foreignKeys = null)
        {
            if (m_conn == null) OpenConnection();

            string commandString = string.Empty;

            for (int i = 0; i < columns.Length; i++)
            {
                NpgsqlModel.SQLColumn c = columns[i];
                if (i != columns.Length - 1)
                    commandString += $"{c.name} {c.DataTypeString} {c.constraints},";
                else
                    commandString += $"{c.name} {c.DataTypeString} {c.constraints}";
            }

            if (!string.IsNullOrEmpty(tablePrimaryKeyColumn))
                commandString += $",CONSTRAINT {tableName}_pkey PRIMARY KEY({tablePrimaryKeyColumn})";

            if (foreignKeys != null && foreignKeys.Length != 0)
            {
                commandString += ",";

                for (int i = 0; i < foreignKeys.Length; i++)
                {
                    if (i != foreignKeys.Length - 1)
                        commandString += $"CONSTRAINT {tableName}_{foreignKeys[i].columnName}_fkey FOREIGN KEY ({foreignKeys[i].columnName}) REFERENCES {foreignKeys[i].foreignTable} ({foreignKeys[i].columnName}) {foreignKeys[i].optionalConstraints},";
                    else
                        commandString += $"CONSTRAINT {tableName}_{foreignKeys[i].columnName}_fkey FOREIGN KEY ({foreignKeys[i].columnName}) REFERENCES {foreignKeys[i].foreignTable} ({foreignKeys[i].columnName}) {foreignKeys[i].optionalConstraints}";
                }
            }

            commandString = $@"CREATE TABLE {tableName} ({commandString});";

            try
            {
                var command = new NpgsqlCommand(commandString, m_conn);
                command.ExecuteNonQuery();

                var txt = $"Table correctly created: {commandString}";

                OnCommandExecuted?.Invoke("CREATE TABLE", txt);

                if (logging)
                    Debug.Log(txt);

            }
            catch (Exception ce)
            {
                if (logging)
                {
                    Debug.Log($"Error with command: {commandString}");
                    Debug.Log(ce.ToString());
                }
            }
        }

        /// <summary>
        /// http://www.postgresqltutorial.com/postgresql-drop-table/
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="ifExists"></param>
        /// <param name="cascadeConstraints"></param>
        public override void DropTable(string tableName, bool ifExists = true, bool cascadeConstraints = false)
        {
            if (m_conn == null) OpenConnection();

            var commandString = $"DROP TABLE {(ifExists ? "IF EXISTS" : "")} {tableName} {(cascadeConstraints ? "CASCADE" : "")};";
            try
            {
                var command = new NpgsqlCommand(commandString, m_conn);
                command.ExecuteNonQuery();

                var txt = $"Table correctly dropped: {commandString}";

                OnCommandExecuted?.Invoke("DROP TABLE", txt);

                if (logging)
                    Debug.Log(txt);
            }
            catch (Exception ce)
            {
                if (logging)
                {
                    Debug.Log($"Error with command: {commandString}");
                    Debug.Log(ce.ToString());
                }
            }
        }
        #endregion

        #region Insert and Update methods
        public async Task<int> InsertIntoAsync(string tableName, string[] columns, string[] values, bool autoincrement)
        {
            if (m_conn == null) OpenConnection();

            var commandString = $"INSERT INTO {tableName} (";

            for (int i = 0; i < columns.Length; i++)
            {
                if (autoincrement && i == 0)
                    continue;

                commandString += columns[i];

                if (i != columns.Length - 1)
                    commandString += ",";
                else
                    commandString += ")";
            }

            commandString += " VALUES (";

            for (int i = 0; i < values.Length; i++)
            {
                if (autoincrement && i == 0)
                    continue;

                commandString += $"'{values[i]}'";

                if (i != values.Length - 1)
                    commandString += ",";
                else
                    commandString += ")";
            }

            if (autoincrement && columns.Length > 0)
                commandString += $" RETURNING {columns[0]};";
            else
                commandString += ";";

            try
            {
                var command = new NpgsqlCommand(commandString, m_conn);

                if (!autoincrement)
                {
                    await command.ExecuteNonQueryAsync();

                    var txt = $"Row correctly inserted: {commandString}";
                    OnCommandExecuted?.Invoke("INSERT INTO", txt);

                    if (logging)
                        Debug.Log(txt);
                }
                else
                {
                    var lastInsertedId = (int)(await command.ExecuteScalarAsync());

                    var txt = $"Row correctly inserted; id: {lastInsertedId}, command: {commandString}";

                    OnCommandExecuted?.Invoke("INSERT INTO", txt);

                    if (logging)
                        Debug.Log(txt);

                    return lastInsertedId;
                }
            }
            catch (Exception ce)
            {
                if (logging)
                {
                    Debug.Log($"Error with command: {commandString}");
                    Debug.Log(ce.ToString());
                }
            }

            return 0;
        }
        #endregion

    }
}
#endif