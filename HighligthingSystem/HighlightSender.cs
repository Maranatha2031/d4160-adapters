﻿#if HIGHLIGHTING_SYSTEM
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using d4160.ECS;
using HighlightingSystem;

namespace d4160.Adapters.HighlightingSystem
{
	/// For Entities that can make highlight other entities, so store different 'profiles' of highlight properties
	public abstract class HighlightSender<T> : ComponentDataWrapper<T> where T : IComponentData
	{
		public abstract void Highligth(Highlighter lighter, int highlightIdx = 0);
	}
}
#endif
