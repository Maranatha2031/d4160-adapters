﻿#if HIGHLIGHTING_SYSTEM
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using d4160.ECS;

namespace d4160.Adapters.HighlightingSystem
{
	public enum HighlightMode
	{
		OneFrame,
		Constant,
		Flashing,
		Occluder,
		SeeThrough
	}

	public enum HighlightAction
	{
		On,
		OnImmediate,
		Off,
		OffImmediate,
		Switch,
		SwitchImmediate
	}

	[System.Serializable]
	public struct HighlightOptions : IHighlightOptions
	{
		public Color color;
		public HighlightMode mode;
		public HighlightAction action;
		public float fadeTransitionDuration;
	}

	[System.Serializable]
	public struct HighlightBasicOptions : IHighlightOptions
	{
		public Color color;
		public HighlightMode mode;
		public HighlightAction action;
		//public float fadeTransitionDuration; Default transition of 0.25
	}

	[System.Serializable]
	public struct HighlightSimpleOptions : IHighlightOptions
	{
		//public Color color; Don't change the default color
		public HighlightMode mode;
		public HighlightAction action;
		//public float fadeTransitionDuration; Default transition of 0.25
	}
}
#endif