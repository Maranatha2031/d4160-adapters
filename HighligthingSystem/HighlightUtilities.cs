#if HIGHLIGHTING_SYSTEM
using HighlightingSystem;
using UnityEngine;
using d4160.ECS;

namespace d4160.Adapters.HighlightingSystem
{
    public static class HighlightUtilities
    {
		/// <summary>
		/// Change color only for OneFrame and Constant modes
		/// </summary>
        public static void ChangeColor(Highlighter h, Color c, HighlightMode mode)
		{
			switch(mode)
			{
				case HighlightMode.OneFrame:
					h.OnParams(c);
				break;
				case HighlightMode.Constant:
					h.ConstantParams(c);
				break;
			}
		}

		/// <summary>
		/// Change params of Flashing
		/// </summary>
        public static void ChangeFlashingParams(Highlighter h, Color c1, Color c2, float frequency)
		{
			h.FlashingParams(c1, c2, frequency);
		}

		public static void Highlight(Highlighter h, HighlightMode mode, HighlightAction action, Color c = default(Color), float transition = 0.25f)
		{
			switch(mode)
			{
				case HighlightMode.OneFrame:
					HighlightOneFrame(h, action, c);
				break;
				case HighlightMode.Constant:
					HighlightConstant(h, action, c, transition);
				break;
				case HighlightMode.Flashing:
					HighlightFlashing(h, action);
				break;
				case HighlightMode.Occluder:
					HighlightOccluder(h, action);
				break;
				case HighlightMode.SeeThrough:
					HighlightSeeThrough(h, action);
				break;
			}
		}

		public static void HighlightOneFrame(Highlighter h, HighlightAction action, Color c)
		{
			switch (action)
			{
				case HighlightAction.On:
					h.On(c);
				break;
				case HighlightAction.OnImmediate:
				break;
				case HighlightAction.Off:
				break;
				case HighlightAction.OffImmediate:
				break;
				case HighlightAction.Switch:
				break;
				case HighlightAction.SwitchImmediate:
				break;
			}
		}

		public static void HighlightConstant(Highlighter h, HighlightAction action, Color c, float transition = 0.25f)
		{
			switch (action)
			{
				case HighlightAction.On:
					h.ConstantOn(c, transition);
				break;
				case HighlightAction.OnImmediate:
					h.ConstantOnImmediate(c);
				break;
				case HighlightAction.Off:
					h.ConstantOff(transition);
				break;
				case HighlightAction.OffImmediate:
					h.ConstantOffImmediate();
				break;
				case HighlightAction.Switch:
					Debug.Log("Highlight");
					h.ConstantSwitch(transition);
				break;
				case HighlightAction.SwitchImmediate:
					h.ConstantSwitchImmediate();
				break;
			}
		}

		public static void HighlightFlashing(Highlighter h, HighlightAction action)
		{
			switch (action)
			{
				case HighlightAction.On:
					h.FlashingOn();
				break;
				case HighlightAction.OnImmediate:
				break;
				case HighlightAction.Off:
					h.FlashingOff();
				break;
				case HighlightAction.OffImmediate:
				break;
				case HighlightAction.Switch:
					h.FlashingSwitch();
				break;
				case HighlightAction.SwitchImmediate:
				break;
			}
		}

		public static void HighlightOccluder(Highlighter h, HighlightAction action)
		{
			switch (action)
			{
				case HighlightAction.On:
					h.occluder = true;
				break;
				case HighlightAction.OnImmediate:
				break;
				case HighlightAction.Off:
					h.occluder = false;
				break;
				case HighlightAction.OffImmediate:
				break;
				case HighlightAction.Switch:
					h.occluder = !h.occluder;
				break;
				case HighlightAction.SwitchImmediate:
				break;
			}
		}

		public static void HighlightSeeThrough(Highlighter h, HighlightAction action)
		{
			switch (action)
			{
				case HighlightAction.On:
					h.seeThrough = true;
				break;
				case HighlightAction.OnImmediate:
				break;
				case HighlightAction.Off:
					h.seeThrough = false;
				break;
				case HighlightAction.OffImmediate:
				break;
				case HighlightAction.Switch:
					h.seeThrough = !h.seeThrough;
				break;
				case HighlightAction.SwitchImmediate:
				break;
			}
		}

		public static void Highlight(IHighlightOptions highlight)
		{
		}
    }
}
#endif