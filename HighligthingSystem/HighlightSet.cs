﻿#if HIGHLIGHTING_SYSTEM
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using d4160.ECS;
using HighlightingSystem;

namespace d4160.Adapters.HighlightingSystem
{
	/// For Entities that can store a set of different 'profiles' of highlight properties
	public abstract class HighlightSet<T> : ComponentDataWrapper<T> where T : IComponentData
	{
		protected Highlighter m_highlight;

		protected override void CatchComponents()
        {
            m_highlight = GetComponent<Highlighter>();

            if (m_highlight == null)
                m_highlight = GetComponentInParent<Highlighter>();

            if (m_highlight == null)
                m_highlight = transform.parent.GetComponentInParent<Highlighter>();

            if (m_highlight == null)
                Debug.LogError("Entity missing for this component: " + name);
        }

		public abstract void Highligth(int highlightIdx = 0);
	}
}
#endif