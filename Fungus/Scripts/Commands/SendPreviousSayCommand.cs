﻿#if FUNGUS

//#define DEBUG_SendPreviousSayCommand

namespace d4160.Adapters._Fungus
{
    using UnityEngine;
    using Fungus;
    using DefaultGame;

    /// <summary>
    /// Writes text in a dialog box.
    /// </summary>
    [CommandInfo("Custom",
                 "SendPreviousSayCommand",
                 "Try to convert the previous command to Say and send the CharacterStoryTest to the receiver (parent of the flowchart)")]
    [AddComponentMenu("")]
    public class SendPreviousSayCommand : Command
    {
        [SerializeField] protected string m_characterName;

        public override void OnEnter()
        {
            if (ParentBlock)
            {
                var previous = ParentBlock.CommandList[CommandIndex - 1];
                var say = previous as Say;

                if (say)
                {
                    var cst = GetFlowchart().transform.parent.GetComponent<ICharacterStoryTextReceiver>();

                    if (cst != null)
                    {
                        cst.SendCharacterStoryText(m_characterName, say.StoryText);
                    }
                }
            }

            Continue();
        }
    }
}
#endif