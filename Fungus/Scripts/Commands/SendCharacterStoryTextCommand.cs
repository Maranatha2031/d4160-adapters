﻿#if FUNGUS

//#define DEBUG_SendPreviousSayCommand

namespace d4160.Adapters._Fungus
{
    using UnityEngine;
    using Fungus;
    using DefaultGame;

    /// <summary>
    /// Writes text in a dialog box.
    /// </summary>
    [CommandInfo("Custom",
                 "SendCharacterStory",
                 "Try to convert the previous command to Menu and send the CharacterStoryTest to the receiver (parent of the flowchart)")]
    [AddComponentMenu("")]
    public class SendCharacterStoryTextCommand : Command
    {
        [SerializeField] protected string m_characterName;
        [TextArea(4, 10)]
        [SerializeField] protected string m_storyText;

        public override void OnEnter()
        {

            var cst = GetFlowchart().transform.parent.GetComponent<ICharacterStoryTextReceiver>();

            if (cst != null)
            {
                cst.SendCharacterStoryText(m_characterName, m_storyText);
            }

            Continue();
        }

        public override string GetSummary()
        {
            string namePrefix = "";
            if (!string.IsNullOrEmpty(m_characterName))
            {
                namePrefix = m_characterName + ": ";
            }

            return namePrefix + "\"" + m_storyText + "\"";
        }

        public override Color GetButtonColor()
        {
            return new Color32(100, 225, 235, 255);
        }
    }
}
#endif