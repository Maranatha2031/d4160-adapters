﻿#if FUNGUS
namespace d4160.Adapters._Fungus
{
    public enum FlowchartType
    {
        Narration,
        Conversation,
        Question
    }
}
#endif