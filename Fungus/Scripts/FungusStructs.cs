﻿#if FUNGUS
namespace d4160.Adapters._Fungus
{
    using Fungus;

    [System.Serializable]
    public struct FlowchartBlock
    {
        public Flowchart flowchart;
        public Block startingBlock;
    }
}
#endif
