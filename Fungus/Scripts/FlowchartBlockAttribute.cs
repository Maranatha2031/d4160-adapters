﻿namespace d4160.Adapters._Fungus
{
    using UnityEngine;

    public class FlowchartBlockAttribute : PropertyAttribute
    {
        public string FlowchartBlockArray { get; set; }

        public FlowchartBlockAttribute(string flowchartBlockArrayPropertyName)
        {
            FlowchartBlockArray = flowchartBlockArrayPropertyName;
        }
    }
}