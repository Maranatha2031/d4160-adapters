﻿#if CINEMACHINE
using d4160.ECS;
using UnityEngine;

namespace d4160.Adapters.Cinemachine
{
    [System.Serializable]
    public struct CMTargetGroupOptions
    {
        public int index;
        public float weight;
        public float radius;
    }

    [System.Serializable]
    public struct CMTargetGroupMinMax
    {
        public int index;
        public Vector2 weightMinMax;
        public Vector2 radiousMinMax;
    }

    [System.Serializable]
    public struct CMTargetGroupOptionsArray
    {
        public CMTargetGroupOptions[] targetGroupOptions;
    }

    [System.Serializable]
    public struct CMTargetGroupMinMaxArray
    {
        public CMTargetGroupMinMax[] minMaxTargetGroups;
    }
}
#endif