﻿namespace DefaultGame
{
    using d4160.Player;
    using d4160.Singletons;
    using Tayx.Graphy;
    using UnityEngine;
    using VRTK.Prefabs.CameraRig.TrackedAlias;
    using Zinnia.Event.Proxy;
    using Zinnia.Rule;

    [RequireComponent(typeof(TrackedAliasFacade))]
    public class PlayerBehavior : Singleton<PlayerBehavior>, IPlayer
    {
        public TransformDataProxyEmitter transformDataProxyEmitter;
        public GameObject leftSelectTeleport;
        public GameObject rightSelectTeleport;
        public EmptyEventProxyEmitter enablePointerVisibilityEmitter;
        public EmptyEventProxyEmitter disablePointerVisibilityEmitter;

        private TrackedAliasFacade _trackedAlias;

        public TrackedAliasFacade TrackedAlias => _trackedAlias;

        protected override void Awake()
        {
            base.Awake();

            _trackedAlias = GetComponent<TrackedAliasFacade>();
        }

        protected void Start()
        {
            if (GraphyManager.Instance)
                GraphyManager.Instance.AudioListener = _trackedAlias.ActiveHeadsetCamera.GetComponent<AudioListener>();
        }
    }
}