﻿namespace d4160.Adapters.SteamVR
{
    using UnityEngine;
    using UnityEngine.UI;
    using Valve.VR.InteractionSystem;
    using Zinnia.Data.Type;

    //-------------------------------------------------------------------------
    [RequireComponent( typeof( Interactable ) )]
	public class UIElementBehaviour : UIElement
	{
        protected override void HandHoverUpdate(Hand hand)
        {
            if ((hand.uiInteractAction != null && hand.uiInteractAction.GetStateDown(hand.handType)) || Input.GetMouseButtonDown(0))
            {
                InputModule.instance.Submit(gameObject);
                ControllerButtonHints.HideButtonHint(hand, hand.uiInteractAction);
            }
        }

        
    }
}
