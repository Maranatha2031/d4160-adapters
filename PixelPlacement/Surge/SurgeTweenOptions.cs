#if SURGE
using Pixelplacement;

namespace d4160.Adapters.Pixelplacement
{
    using ECS.Component;
    using UnityEngine;
    
    [System.Serializable]
    public struct SurgeMaterialTweenOptions<T> : ITweenOptions
    {
        public T from;
        public T to;
        public string matParam;
        public float duration;
        public TweenEase ease;
    }

    [System.Serializable]
    public struct SurgeMaterialFloatTweenOptions : ITweenOptions
    {
        public float from;
        public float to;
        public string matParam;
        public float duration;
        public TweenEase ease;
    }

    [System.Serializable]
    public struct SurgeMaterialFloatSingleTweenOptions : ITweenOptions
    {
        public float to;
        public string matParam;
        public float duration;
        public TweenEase ease;
    }

    [System.Serializable]
    public struct SurgeMaterialSimpleTweenOptions : ITweenOptions
    {
        public string matParam;
        public float duration;
        public TweenEase ease;
    }

    [System.Serializable]
    public struct SurgeTransformTweenOptions : ITweenOptions
    {
        public Transform from;
        public Transform to;
        public float duration;
        public TweenEase ease;
    }

    [System.Serializable]
    public struct SurgeTransformSingleTweenOptions : ITweenOptions
    {
        public Transform to;
        public float duration;
        public TweenEase ease;
    }

    [System.Serializable]
    public struct SurgeFloatTweenOptions : ITweenOptions
    {
        public float from;
        public float to;
        public float duration;
        public TweenEase ease;

        private AnimationCurve _easeCurve;

        public AnimationCurve EaseCurve => _easeCurve;

        public void CalculateEaseCurve()
        {
            _easeCurve = SurgeUtilities.GetAnimationCurve(ease);
        }
    }

    [System.Serializable]
    public struct SurgeVector3TweenOptions : ITweenOptions
    {
        public Vector3 from;
        public Vector3 to;
        public float duration;
        public TweenEase ease;
    }

    /// <summary>
    /// Only with 'to' property, also duration and ease
    /// </summary>
    [System.Serializable]
    public struct SurgeVector3SingleTweenOptions : ITweenOptions
    {
        public Vector3 to;
        public float duration;
        public TweenEase ease;
    }

    /// <summary>
    /// Only with 'to' property, also duration, ease and delay
    /// </summary>
    [System.Serializable]
    public struct SurgeVector3SingleExtraTweenOptions : ITweenOptions
    {
        public Vector3 to;
        public float duration;
        public TweenEase ease;
        public float delay;
    }

    [System.Serializable]
    public struct SurgeTweenOptions<T> : ITweenOptions
    {
        public T from;
        public T to;
        public float duration;
        public TweenEase ease;
        public float delay;
        public Tween.LoopType loopType;
        public bool obeyTimeScale;
    }

    [System.Serializable]
    public struct SurgeSimpleTweenOptions : ITweenOptions
    {
        public float duration;
        public TweenEase ease;

        private AnimationCurve _easeCurve;

        public AnimationCurve EaseCurve => _easeCurve;

        public void CalculateEaseCurve()
        {
            _easeCurve = SurgeUtilities.GetAnimationCurve(ease);
        }
    }

    [System.Serializable]
    public struct SurgeSimpleExtraTweenOptions : ITweenOptions
    {
        public float duration;
        public TweenEase ease;
        public float delay;
    }
    
    public enum TweenEase
    {
        Bounce,
        In,
        InBack,
        InOut,
        InOutBack,
        InOutStrong,
        InStrong,
        Linear,
        Out,
        OutBack,
        OutStrong,
        Spring,
        Wobble
    }
}
#endif