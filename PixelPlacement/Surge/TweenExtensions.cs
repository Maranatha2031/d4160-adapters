﻿#if SURGE

using Pixelplacement.TweenSystem;

namespace d4160.Adapters.Pixelplacement
{
    using System;
    using UnityEngine;
    using static global::Pixelplacement.Tween;

    public class TweenExtensions
    {
        /// <summary>
        /// Changes the named float property of a Material's shader.
        /// </summary>
        public static TweenBase ShaderHashFloat(Material target, int property, float endValue, float duration, float delay, AnimationCurve easeCurve = null, LoopType loop = LoopType.None, Action startCallback = null, Action completeCallback = null, bool obeyTimescale = true)
        {
            ShaderHashFloat tween = new ShaderHashFloat(target, property, endValue, duration, delay, obeyTimescale, easeCurve, loop, startCallback, completeCallback);
            SendTweenForProcessing(tween, true);
            return tween;
        }

        /// <summary>
        /// Changes the named float property of a Material's shader.
        /// </summary>
        public static TweenBase ShaderHashFloat(Material target, int property, float startValue, float endValue, float duration, float delay, AnimationCurve easeCurve = null, LoopType loop = LoopType.None, Action startCallback = null, Action completeCallback = null, bool obeyTimescale = true)
        {
            target.SetFloat(property, startValue);

            ShaderHashFloat tween = new ShaderHashFloat(target, property, endValue, duration, delay, obeyTimescale, easeCurve, loop, startCallback, completeCallback);
            SendTweenForProcessing(tween, true);
            return tween;
        }

        static void SendTweenForProcessing(TweenBase tween, bool interrupt = false)
        {
            if (!Application.isPlaying)
            {
                //Tween can not be called in edit mode!
                return;
            }

            if (interrupt && tween.Delay == 0)
            {
                StopInstanceTargetType(tween.targetInstanceID, tween.tweenType);
            }

            Instance.ExecuteTween(tween);
        }

        static void StopInstanceTargetType(int id, TweenType type)
        {
            for (int i = 0; i < activeTweens.Count; i++)
            {
                if (activeTweens[i].targetInstanceID == id && activeTweens[i].tweenType == type)
                {
                    activeTweens[i].Stop();
                }
            }
        }
    }
}
#endif