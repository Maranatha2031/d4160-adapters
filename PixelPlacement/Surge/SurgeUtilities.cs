#if SURGE
using Pixelplacement;
using UnityEngine;

namespace d4160.Adapters.Pixelplacement
{
    public static class SurgeUtilities
    {
        public static AnimationCurve GetAnimationCurve(TweenEase ease)
        {
            switch (ease)
            {
                case TweenEase.Bounce:
                    return Tween.EaseBounce;
                case TweenEase.In:
                    return Tween.EaseIn;
                case TweenEase.InBack:
                    return Tween.EaseInBack;
                case TweenEase.InOut:
                    return Tween.EaseInOut;
                case TweenEase.InOutBack:
                    return Tween.EaseInOutBack;
                case TweenEase.InOutStrong:
                    return Tween.EaseInOutStrong;
                case TweenEase.InStrong:
                    return Tween.EaseInStrong;
                case TweenEase.Linear:
                    return Tween.EaseLinear;
                case TweenEase.Out:
                    return Tween.EaseOut;
                case TweenEase.OutBack:
                    return Tween.EaseOutBack;
                case TweenEase.OutStrong:
                    return Tween.EaseOutStrong;
                case TweenEase.Spring:
                    return Tween.EaseSpring;
                case TweenEase.Wobble:
                    return Tween.EaseWobble;
                default:
                    return Tween.EaseLinear;
            }
        }
    }
}
#endif