﻿#if SURGE

using Pixelplacement;
using Pixelplacement.TweenSystem;
using System;
using UnityEngine;

namespace d4160.Adapters.Pixelplacement
{
    class ShaderHashFloat : TweenBase
    {
        //Public Properties:
        public float EndValue { get; private set; }

        //Private Variables:
        Material _target;
        float _start;
        int _property;

        //Constructor:
        public ShaderHashFloat(Material target, int property, float endValue, float duration, float delay, bool obeyTimescale, AnimationCurve curve, Tween.LoopType loop, Action startCallback, Action completeCallback)
        {
            //set essential properties:
            SetEssentials(Tween.TweenType.ShaderFloat, target.GetInstanceID(), duration, delay, obeyTimescale, curve, loop, startCallback, completeCallback);

            //catalog custom properties:
            _target = target;
            _property = property;
            EndValue = endValue;
        }

        //Processes:
        protected override bool SetStartValue()
        {
            _start = _target.GetFloat(_property);
            if (_target == null) return false;
            return true;
        }

        protected override void Operation(float percentage)
        {
            float calculatedValue = TweenUtilities.LinearInterpolate(_start, EndValue, percentage);
            _target.SetFloat(_property, calculatedValue);
        }

        //Loops:
        public override void Loop()
        {
            ResetStartTime();
            _target.SetFloat(_property, _start);
        }

        public override void PingPong()
        {
            ResetStartTime();
            _target.SetFloat(_property, EndValue);
            EndValue = _start;
            _start = _target.GetFloat(_property);
        }
    }
}
#endif